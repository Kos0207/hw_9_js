const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
function getList(arr, parent = document.body) {
  let ul = document.createElement("ul");
  arr.forEach((el) => {
    let li = document.createElement("li");
    li.innerHTML = el;
    ul.append(li);
  });

  parent.append(ul);
  console.log(ul);
  setTimeout(() => ul.remove(), 3000);
}
getList(arr, document.querySelector(".container"));

const arr2 = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];
function getList2(arr, parent = document.body) {
  let ul = document.createElement("ul");
  arr.forEach((el) => {
    let li = document.createElement("li");
    if (Array.isArray(el)) {
      getList2(el, li);
    } else {
      li.innerHTML = el;
    }
    ul.append(li);
  });

  parent.append(ul);
  console.log(ul);
  setTimeout(() => ul.remove(), 3000);
}
getList2(arr2, document.querySelector(".container2"));